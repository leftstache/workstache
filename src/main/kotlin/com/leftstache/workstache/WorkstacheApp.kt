package com.leftstache.workstache

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication


@SpringBootApplication
open class WorkstacheApp

fun main(args: Array<String>) {
    SpringApplication.run(WorkstacheApp::class.java, *args)
}