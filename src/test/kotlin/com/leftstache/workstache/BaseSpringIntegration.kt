package com.leftstache.workstache

import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest(classes = arrayOf(WorkstacheApp::class))
@ActiveProfiles(profiles = arrayOf("test"))
abstract class BaseSpringIntegration
